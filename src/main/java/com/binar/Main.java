package com.binar;

import com.binar.controller.ReadWriteFileController;
import com.binar.model.Student;
import com.binar.service.ReadWriteFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        String nasi = "Makan" + "Nasi";
//        System.out.println("pake + : " + nasi);
//
//        String nasi2 = "Makan".concat("Nasi");
//        System.out.println("pake concat() : " + nasi2);
//
//        StringBuffer stringBuffer = new StringBuffer();
//        stringBuffer.append("Backend ");
//        stringBuffer.append("Java");
//        System.out.println("pake stringBuffer : " + stringBuffer.toString());
//
//        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append("Pake ");
//        stringBuilder.append("String Builder");
//        System.out.println("pake stringBuilder : " + stringBuilder.toString());
//
//        Date date = new Date();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        System.out.println(sdf.format(date));
//
//        Math.
//
//        // \s \t , .
//        String[] terpisah = stringBuilder.toString().split(" ");
//        for(String txt : terpisah) {
//            System.out.println(txt);
//        }
        ReadWriteFileController rwc = new ReadWriteFileController();
        rwc.startReadWrite();
    }
}
