package com.binar.service;

import com.binar.model.Student;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadWriteFileImpl implements ReadWriteFile {

    @Override
    public void readFile(String filePath, String delimiter) {
        try {
            File file = new File(filePath);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            String[] tempArr;
//            List<String> usrBej1 = new ArrayList<>();
            List<Student> bej1 = new ArrayList<>();
            while((line = br.readLine()) != null) {
                Student student = new Student();
                tempArr = line.split(delimiter);
                student.setFirstName(tempArr[0]);
                student.setLastName(tempArr[1]);
                student.setRole(tempArr[2]);
                bej1.add(student);
            }
            int lines = 0;
            for (Student student : bej1) {
                lines++;
                if(lines == 4) {
                    System.out.println("Nama " + student.getFirstName() + " " + student.getLastName() + " sebagai " + student.getRole());
                }
            }
//            for(int i = 0; i < bej1.size(); i++) {
//                for(int j = 0; j < usrBej1.size(); j++) {
//                    System.out.print(bej1.get(i).get(j) + " ");
//                }
//                System.out.println();
//            }
            br.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void writeFile(String filePath) {
        try {
            File file = new File(filePath);
            if(file.createNewFile()) {
                System.out.println("new File is being created");
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write("Kelas ini dimulai jam 19.00");
            bwr.newLine();
            bwr.write("Kelas selesai di jam 22.00");
            bwr.flush();
            bwr.close();
            System.out.println("Success writing to a file");
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
