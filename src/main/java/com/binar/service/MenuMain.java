package com.binar.service;

public class MenuMain extends Menu {
    private static final String FILE_PATH_READ = "src/main/resources/test.txt";
    private static final String FILE_PATH_WRITE = "src/main/resources/write.txt";

//    ReadWriteFile file;

    @Override
    public void showMenu() {
        ReadWriteFile file = new ReadWriteFileImpl();
        MenuDone done = new MenuDone();
        System.out.println("Silahkan pilih : \n1. Read File \n2. Write file \n0. Exit");
        switch(promptInput()) {
            case 0:
                System.out.println("Program sedang ditutup");
                System.exit(0);
                break;
            case 1:
                file.readFile(FILE_PATH_READ, ",");
                done.showMenu();
                break;
            case 2:
                file.writeFile(FILE_PATH_WRITE);
                done.showMenu();
                break;
            default:
                System.out.println("Pilihan tidak dikenali! silahkan pilih lagi");
                this.showMenu();
                break;
        }
    }
}
