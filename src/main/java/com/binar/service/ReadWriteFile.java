package com.binar.service;

public interface ReadWriteFile {
    public void readFile(String filePath, String delimiter);
    public void writeFile(String filePath);
}
