package com.binar.service;

public class MenuDone extends Menu{
    @Override
    public void showMenu() {
        MenuMain main = new MenuMain();
        System.out.println("File telah diproses! Silahkan pilih kembali : \n1. Kembali ke menu utama \n0. Exit");
        switch(promptInput()) {
            case 0:
                System.out.println("Program sedang ditutup");
                System.exit(0);
                break;
            case 1:
                main.showMenu();
                break;
            default:
                System.out.println("Pilihan tidak dikenali! silahkan pilih lagi");
                this.showMenu();
                break;
        }
    }
}
