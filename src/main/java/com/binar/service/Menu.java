package com.binar.service;

import java.util.Scanner;

public abstract class Menu {
    private Scanner input = new Scanner(System.in);

    public byte promptInput() {
        System.out.print("---->");
        return input.nextByte();
    }

    public abstract void showMenu();
}
