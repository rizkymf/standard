package com.binar.model;

public class Mobil {
    private String merek;
    public String tahunRelease;

    public String getMerek() {
        return merek;
    }

    public void setMerek(String merek) {
        this.merek = merek;
    }

    public String getTahunRelease() {
        return tahunRelease;
    }

    public void setTahunRelease(String tahunRelease) {
        this.tahunRelease = tahunRelease;
    }
}
